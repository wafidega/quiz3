<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
	<title></title>
	<link rel="stylesheet"  href="{{url('/css/app.css')}}">
</head>
<body>
	<h1>Cobaa</h1>
	<div id="app">
		<example-component></example-component>
	</div>
<script src="{{url('/js/app.js')}}"></script>
</body>
</html>